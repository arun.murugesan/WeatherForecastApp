app.controller('homeCtrl', function ($scope, $http, $rootScope, $timeout) {
    $scope.weatherUrl = "http://api.openweathermap.org/data/2.5/forecast";
    $scope.apiKey = 'dc340760b4e23f6ae71bb820e441d936';
    $scope.location = "Bangalore,In";
    $scope.weatherData = {};
    $scope.hoursData = [];
    $scope.daySwiper = {};
    $scope.hourSwiper = {};
    $scope.fetchWeatherData = function () {
        console.log("webser");
        $http.get($scope.weatherUrl + '?q=' + $scope.location + '&units=metric&appid=' + $scope.apiKey).then(function (response) {
            console.log(response);
            $scope.weatherData = _.groupBy(response.data.list, function (item) { return item.dt_txt.substring(0, 10) });
          $scope.onDaySelection($scope.weatherData[Object.keys($scope.weatherData)[0]]);
        }, function (err) {
            console.error(err);
        });

    };
   
    $scope.onReadySwiper = function (swiper) {
        swiper.initObservers();
    }
    $scope.fetchWeatherData();
    $scope.onDaySelection = function (hoursData) {
        console.log(hoursData);
        $scope.hoursData = hoursData;
        $scope.hourSwiper.slideTo(0);
    };
});