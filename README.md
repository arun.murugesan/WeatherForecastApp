# Weather Forecast

This application shows weather forecast data for next 5 days with 3 hours interval for different location.

## How to run this application

Follow the below steps to run this application.

 Clone the project.
 
 ```
 git clone git@gitlab.com:arun.murugesan/WeatherForecastApp.git
 cd WeatherForecastApp
 ```

 Install NPM packages.

```
npm install
```

 Run the application in browser.

```
npm start
```

## Future Enhancements

1. Better UX design.
2. Unit testing.
3. Presenting more data like cloudiness, wind, rain.