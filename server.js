var express=require('express');
var app=express();
var bodyparser=require("body-parser");
var cookieParser = require('cookie-parser');
var open = require('open');
var httpport =  3000;


app.use(express.static(__dirname+"/public"));
app.use(cookieParser());
app.use(bodyparser.json());

app.listen(httpport);
console.log("Weather forecasting app is running at http://localhost:" + httpport);
open('http://localhost:'+httpport);
